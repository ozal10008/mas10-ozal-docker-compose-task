package az.ingress.ms10ozaldockercomposetask.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "counter")
public class Counter {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "counter")
    private Long counter;
}
