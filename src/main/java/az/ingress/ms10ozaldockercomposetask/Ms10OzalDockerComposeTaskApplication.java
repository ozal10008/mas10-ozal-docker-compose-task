package az.ingress.ms10ozaldockercomposetask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ms10OzalDockerComposeTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ms10OzalDockerComposeTaskApplication.class, args);
	}

}
