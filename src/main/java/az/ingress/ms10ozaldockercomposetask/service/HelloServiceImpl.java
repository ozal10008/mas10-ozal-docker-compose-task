package az.ingress.ms10ozaldockercomposetask.service;

import az.ingress.ms10ozaldockercomposetask.entity.Counter;
import az.ingress.ms10ozaldockercomposetask.repository.CounterRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class HelloServiceImpl implements HelloService{

    private final CounterRepository counterRepository;

    @Override
    public String sayHello() {
        Long lastId=counterRepository.findTopByOrderByIdDesc().getId();
        Counter counter=new Counter();
        counter.setCounter(lastId+1L);
        counterRepository.save(counter);
        return "Hello, the counter is "+lastId;
    }
}
